from pyfirmata import Arduino, util
from tkinter import *
import time
from PIL import Image , ImageTk

sizex_circ=50
sizey_circ=50

canvas_width =  500
canvas_height = 500

width=int(150/2)
height=int(150/3)

placa = Arduino ('COM3')
it = util.Iterator(placa)
it.start()

led1= placa.get_pin('d:13:o')
led2= placa.get_pin('d:12:o')
led3= placa.get_pin('d:11:o')
led4= placa.get_pin('d:10:o')
led5= placa.get_pin('d:9:o')
led6= placa.get_pin('d:8:o')

ventana = Tk()
ventana.geometry('600x600')
ventana.configure(bg = 'white')
ventana.title("Bienvenidos a las UI")
texto = Label(ventana, text="LED PWM", bg='cadet blue1', font=("Arial Bold", 25), fg="white")

draw = Canvas(ventana, width=canvas_width, height=canvas_height)
draw.place(x = 10,y = 100)

led1_draw=draw.create_oval(10,10,10+sizex_circ,10+sizey_circ,fill="red")
led2_draw=draw.create_oval(70,10,70+sizex_circ,10+sizey_circ,fill="white")
led3_draw=draw.create_oval(130,10,130+sizex_circ,10+sizey_circ,fill="white")
led4_draw=draw.create_oval(190,10,190+sizex_circ,10+sizey_circ,fill="white")
led5_draw=draw.create_oval(250,10,250+sizex_circ,10+sizey_circ,fill="white")
led6_draw=draw.create_oval(310,10,310+sizex_circ,10+sizey_circ,fill="white")

#########################################

img=Image.open("C:/Users/LENOVO/Desktop/herramientas/usa.png")#abre la imagen
img=img.resize((width,height))#se cambia el tamaño
imagen=ImageTk.PhotoImage(img) #almacena la imagen en un objeto
b=Label(ventana, image=imagen).place(x=200,y=10) #coloca la imagen en el objeto
#############################################
titulo=Label(ventana,text="PUNTO 2",font=("Rockwell Extra Bold",18),fg="black")
titulo.place(x=20,y=20)
titulo.configure(bg='pale turquoise')
def led_pwm(valor):

    global led1_draw
    global led2_draw
    global led3_draw
    global led4_draw
    global led5_draw
    global led6_draw
    
    if(int(valor)<=36):
        led1.write(1)
        led2.write(0)
        led3.write(0)
        led4.write(0)
        led5.write(0)
        led6.write(0)

        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="white")
        draw.itemconfig(led3_draw, fill="white")
        draw.itemconfig(led4_draw, fill="white")
        draw.itemconfig(led5_draw, fill="white")
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()
        
    if(int(valor)>36 and int(valor)<=72):
        led1.write(1)
        led2.write(1)
        led3.write(0)
        led4.write(0)
        led5.write(0)
        led6.write(0)
        
        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="yellow")
        draw.itemconfig(led3_draw, fill="white")
        draw.itemconfig(led4_draw, fill="white")
        draw.itemconfig(led5_draw, fill="white")
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()

    if(int(valor)>72 and int(valor)<=108):
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(0)
        led5.write(0)
        led6.write(0)
        
        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="yellow")
        draw.itemconfig(led3_draw, fill="yellow")
        draw.itemconfig(led4_draw, fill="white")
        draw.itemconfig(led5_draw, fill="white")
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()

    if(int(valor)>108 and int(valor)<=144):
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(1)
        led5.write(0)
        led6.write(0)
        
        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="yellow")
        draw.itemconfig(led3_draw, fill="yellow")
        draw.itemconfig(led4_draw, fill="green")
        draw.itemconfig(led5_draw, fill="white")
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()

    if(int(valor)>144 and int(valor)<=180):
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(1)
        led5.write(1)
        led6.write(0)
        
        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="yellow")
        draw.itemconfig(led3_draw, fill="yellow")
        draw.itemconfig(led4_draw, fill="green")
        draw.itemconfig(led5_draw, fill="green")
        draw.itemconfig(led6_draw, fill="white")
        ventana.update()
        
    if(int(valor)>180):
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(1)
        led5.write(1)
        led6.write(1)
        
        draw.itemconfig(led1_draw, fill="red")
        draw.itemconfig(led2_draw, fill="yellow")
        draw.itemconfig(led3_draw, fill="yellow")
        draw.itemconfig(led4_draw, fill="green")
        draw.itemconfig(led5_draw, fill="green")
        draw.itemconfig(led6_draw, fill="yellow")
        ventana.update()
        
lum = Scale(ventana, 
            command=led_pwm, 
            from_= 1, 
            to = 216, 
            orient = 
            HORIZONTAL, 
            length = 300, 
            label = "INTENSIDAD",
            bg = 'cadet blue1',
            font=("Arial Bold", 15),
            fg="white"

            )
lum.grid(padx=20, pady=20,column=0, row=1)
lum.place(x=20,y=180)

ventana.mainloop()
